# debut

`/deɪˈbjuː/`

From [Wiktionary](https://en.wiktionary.org/wiki/debut):

> From French début, from Middle French, derivative of débuter ("to move, begin").

debut is an Ansible playbook that will get you started on Debian.

## Preinstallation

Run `scripts/pre-install.sh`. This will install the minimum set of
dependencies to be able to run the Ansible playbook.

## Lazy usage

Lazy people just can run:

```sh
make
```

Or if you want to specify a [tag](#tags), run:

```sh
make <tag>
```

If you want to ask for the `sudo` password also specify `sudo`
(order doesn't matter):

```sh
make <tag> sudo
```

To have verbose output, also pass the `verbose` argument to `make`.

## Usage

```sh
ansible-playbook playbook.yml -K
```

The parameter `-K` will make it ask for your password to execute `sudo` commands.

### Tags

It is possible to only execute a subset of the installation steps, e.g.:

```sh
# Only install wifi drivers
ansible-playbook playbook.yml -K --tags=wifi

# Skip installation of dropbox
ansible-playbook playbook.yml -K --skip-tags=dropbox

# Print list of available tags
ansible-playbook playbook.yml --list-tags
# ... or
make tags
```

## Inventory

By default only the local machine is in the inventory. You can optionally run
this on remote machines as well.

### Remote GDK

I'm also using this set up my remote GDK machine in the cloud.

Extra utils that need installation:

- Golang

   ```sh
   sudo apt install golang-1.14

   echo 'PATH=$HOME/bin:/usr/lib/go-1.14/bin:$PATH' >> ~/.bashrc
   ```

- Runit: Install runit from source: https://smarden.org/runit/install.html

## Inspiration

This playbook is inspired by [Preamble](https://github.com/pjaspers/preamble)
by @pjaspers. I named it differently because this one runs on Debian,
and his one runs on macOS.

## License

This config is covered under the [Mozilla Public License Version 2.0](LICENSE).
