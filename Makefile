PATH  := $(PATH):$(PWD)/python
SHELL := env PATH=$(PATH) /bin/bash
ANSIBLE_PLAYBOOK := ansible-playbook

PLAYBOOK := playbook.yml

all:
	$(ANSIBLE_PLAYBOOK) $(PLAYBOOK) --ask-become-pass

tags:
	$(ANSIBLE_PLAYBOOK) $(PLAYBOOK) --list-tags $(inventory)

%:
	$(ANSIBLE_PLAYBOOK) $(PLAYBOOK) --tags=$@ $(sudo) $(verbose) $(inventory)

# some noop targets are only used to pass extra command line arguments to ansible
sudo := $(if $(filter sudo,$(MAKECMDGOALS)),--ask-become-pass,)
verbose := $(if $(filter verbose,$(MAKECMDGOALS)),-vvv,)
inventory := --inventory=$(if $(filter rgdk,$(MAKECMDGOALS)),inventory/rgdk,inventory/local)

sudo verbose rgdk:
	@#
