#!/bin/sh

set -e

# Install Ansible locally
mkdir -p python
env PYTHONPATH=$PWD/python easy_install --install-dir $PWD/python ansible

# Run first-run playbook
python/ansible-playbook first-run.yml --become-method=su --become-user=root --ask-become-pass
